package convergencia.vodafone.com.convergencia.web;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import convergencia.vodafone.com.convergencia.config.Config;

/**
 * Executes all http request into the app.
 * @author Flash2Flash
 *
 */
public class HttpClient {
	private static final String TAG = "HttpClient";

	/**
	 * Send an http post request
	 * @param context
	 * @param URL
	 * @param params
	 * @param permitAll
	 * @return {@link JSONObject} with the info returned by the service
	 */
	@SuppressWarnings("deprecation")
	public static JSONObject SendHttpPost(Context context, String URL, HashMap<String, String> params, boolean permitAll) {
		
		long t = System.currentTimeMillis();
		Log.d(TAG, "New Request POST. Service:" + URL);
		URL url;
        String response = "";
        
		try {
			
			if(permitAll){
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
			}
			
			url = new URL(URL);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(Config.HTTP_CONNECTION_TIMEOUT);
            conn.setConnectTimeout(Config.HTTP_CONNECTION_TIMEOUT);
			conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
	        
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
			//Log.e(TAG, "Post Data params:" + getPostDataString(params));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();
            
            if (responseCode == HttpsURLConnection.HTTP_OK) {
            	// Read the content stream
				InputStream instream = conn.getInputStream();
				
				// convert content stream to a String
				String resultString= convertStreamToString(instream);
				instream.close();
				
				// Transform the String into a JSONObject
				try{
					JSONObject jsonObjRecv = new JSONObject(resultString);
					
					return jsonObjRecv;
				}catch(Exception e){
					JSONObject obj = new JSONObject();
					JSONObject error = new JSONObject();
					error.put("code", "000").put("message", "Error en servidor, inténtelo más tarde").put("description", "Error en servidor, inténtelo más tarde");
					obj.put("error", error);
					return obj;
					
				}
            }
            else {
            	JSONObject obj = new JSONObject();
				JSONObject error = new JSONObject();
				error.put("code", "000").put("message", "Error en servidor, inténtelo más tarde").put("description", "Error en servidor, inténtelo más tarde");
				obj.put("error", error);
				return obj;    

            }

		}  catch (SocketTimeoutException e) {
			Log.d(TAG, "SocketTimeoutException - HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]. Service:" + URL);
		} catch (Exception e) {
			Log.w(TAG, "Exception - HTTPResponse received in [" + (System.currentTimeMillis()-t) + "ms]. Service:" + URL);
			e.printStackTrace();
		}
		return null;
	}
	
	private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
	
	/**
	 * To convert the InputStream to String we use the BufferedReader.readLine()
	 * method. We iterate until the BufferedReader return null which means
	 * there's no more data to read. Each line will appended to a StringBuilder
	 * and returned as String.
	 * 
	 * (c) public domain: http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
	 */
	public static String convertStreamToString(InputStream is) {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}