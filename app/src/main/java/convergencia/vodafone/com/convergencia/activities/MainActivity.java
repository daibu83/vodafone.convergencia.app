package convergencia.vodafone.com.convergencia.activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.UncachedSpiceService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import convergencia.vodafone.com.convergencia.R;
import convergencia.vodafone.com.convergencia.utils.HTTPDataHandler;
import convergencia.vodafone.com.convergencia.utils.Utils;
import convergencia.vodafone.com.convergencia.web.UploadMediaRequest;

public class MainActivity extends FragmentActivity {

    String uploadUrl = Utils.getServicesUrl() +  "invitacion-control-acceso/";
    String codeUrl = Utils.getServicesUrl() +  "invitacion-control-acceso/";
    public SpiceManager spiceManager = new SpiceManager(UncachedSpiceService.class);

    public File tempFile;
    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1000;
    private int CAPTURE_QR_ACTIVITY_REQUEST_CODE = 1001;
    public File fFinal;
    public File tmpFile;

    public RelativeLayout step1, step2;
    //private ImageView camera;
    private EditText name;
    private Button cancel,scan,upload, add;
    private TextView codeStatusText;

    private String uploadingName = "";
    private String qrScanned, codeControl = "";

    private static final int NOTIFICATION_ID  = 3058;
    NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;
    private AlertDialog alertDialog;

    private UploadMediaRequest request;

    //JSONObject currentQrs = new JSONObject();
    private int counterQrs = 0, codeStatus, duration;
    private Toast toast;
    private Context mContext;

    ProgressDialog progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // getActionBar().hide();

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder	=	new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.logo_vodafone)
                .setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.logo_vodafone)).getBitmap()))
                .setAutoCancel(true);

        //step1 	= (RelativeLayout)findViewById(R.id.step1);
        step2 	= (RelativeLayout)findViewById(R.id.step2);

        cancel 	= (Button)findViewById(R.id.cancel);
        scan 	= (Button)findViewById(R.id.scan);
        upload 	= (Button)findViewById(R.id.upload);
        add 	= (Button)findViewById(R.id.add);

        codeStatusText = (TextView)findViewById(R.id.codeStatusText);
        name 	= (EditText)findViewById(R.id.name);

        //camera 	= (ImageView) findViewById(R.id.camera);

        /*camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });*/

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cancel();

            }
        });

        scan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openScanIntent();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.length() > 0 ) {

                    codeControl = name.getText().toString();
                    //codeUrl = codeUrl + codeControl;
                    new ProcessJSON().execute(codeUrl + codeControl);
                    //name.setText("");
                }else{
                    mContext = getApplicationContext();
                    duration = Toast.LENGTH_SHORT;

                    toast = Toast.makeText(mContext, "Por favor, introduzca un código", duration);
                    toast.show();
                }
            }
        });

        spiceManager.start(this);
    }

    private void cancel(){
        onBackPressed();
    }
    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        spiceManager.shouldStop();
    }

    /*public void takePhoto(){

        tmpFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constants.IMAGES_DIRECTORY, "tmp.jpg");

        if(tmpFile.isFile() && tmpFile.exists()){
            tmpFile.delete();
            try {
                tmpFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else{
            if(!tmpFile.getParentFile().exists()){
                tmpFile.getParentFile().mkdirs();
            }
            try {
                tmpFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //cameraIntent.putExtra("android.intent.extras.CAMERA_FACING_FRONT", 1);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpFile));
        startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }*/

    private void openScanIntent() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, CAPTURE_QR_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        /*if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                step1.setVisibility(View.GONE);
                step2.setVisibility(View.VISIBLE);

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
            onLowMemory();
            System.gc();
        }*/
        if (requestCode == CAPTURE_QR_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                qrScanned = getQr(contents);
                //addQr(qrScanned);
                name.setText(qrScanned);
            } else {

            }
        }

    }

   /* private void addQr(String qr){
        try {
            if(!qrExists(qr)){
                //currentQrs.put(String.valueOf(counterQrs), qr);
                //currentQrs.put(qr, "");
                //counterQrs++;
                codeControl = qr;
                //codeUrl = codeUrl + codeControl;
                new ProcessJSON().execute(codeUrl + codeControl);
                //codeStatusText.setText("" + codeControl);
                //codeStatusText.setText("Resultado del código: " + codeControl);
                //Log.d("current qr", codeControl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    private String getQr(String url){
        url = url.replace("\n", "");
        url= url.trim();
        return url.substring(url.length() - 7, url.length());
    }

    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[2048];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    /*private void startUpload(){



        String finalName = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()) + "_" + getForFilename() + "_0.jpg";

        fFinal = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constants.IMAGES_DIRECTORY, finalName);

        if(tmpFile != null && tmpFile.isFile() &&  tmpFile.renameTo(fFinal)){
            tmpFile.delete();

            String base64File = Base64.encodeToString(Utils.readFile(fFinal), Base64.DEFAULT);

            HashMap<String,String> params = new HashMap<String,String>();
            params.put("image", base64File);
            params.put("qr", currentQrs.toString());
            request = new UploadMediaRequest(MainActivity.this, uploadUrl, params);
            spiceManager.execute(request, uploadListener);
            //spiceManager.cancel(request);
            initUploadNotification();
            uploadingName = finalName;
            reset();
        }


    }*/

    /*private UploadMediaRequestListener uploadListener = new UploadMediaRequestListener(MainActivity.this){

        @Override
        public void onRequestSuccess(String serviceResult) {

            try {

                JSONObject response = new JSONObject(serviceResult);

                String text = new String();
                String ticker = new String();

                if(!serviceHasError(response)){
                    File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constants.IMAGES_DIRECTORY, uploadingName);
                    String nname = uploadingName.replace("_0.", "_1.");
                    File f1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constants.IMAGES_DIRECTORY, nname);
                    if(f.renameTo(f1)){
                        f.delete();
                    }
                    text = "Imagen subida correctamente";
                    ticker = "Subida terminada";

                }else{
                    text = "Error al subir la imagen";
                    ticker = "Error en la subida";
                }

                uploadingName = "";
                endUploadNotification(text, ticker);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };*/

    private void reset(){
        qrScanned = "";
        //currentQrs = new JSONObject();
        counterQrs = 0;
        name.setText("");
        codeStatusText.setText("");
        codeControl = "";
        /*if(tmpFile != null && tmpFile.exists()){
            tmpFile.delete();
        }*/

        //tmpFile = null;
        /*step2.setVisibility(View.GONE);
        step1.setVisibility(View.VISIBLE);*/
    }

    public void initUploadNotification(){

        mBuilder.setContentTitle("Se está subiendo la última imagen tomada");
        mBuilder.setContentText("Se está subiendo la última imagen tomada");
        mBuilder.setTicker("Subiendo");

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public void endUploadNotification(String text, String ticker){

        mBuilder.setContentTitle(text);
        mBuilder.setContentText(text);
        mBuilder.setTicker(ticker);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }

    /*private boolean qrExists(String value) throws JSONException {
        Iterator<String> keys = currentQrs.keys();
        while (keys.hasNext()) {
            String key = (String)keys.next();
            if ( currentQrs.getString(key) instanceof String ) {
                if (currentQrs.getString(key).equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }*/

    /*private boolean qrExists(String value) throws JSONException {
        Iterator<String> keys = currentQrs.keys();
        while (keys.hasNext()) {
            String key = (String)keys.next();
            if (key.equals(value)) {
                return true;
            }

        }
        return false;
    }*/

    /*private String getForFilename(){
        Iterator<String> keys = currentQrs.keys();
        String name = "@@";
        while (keys.hasNext()) {
            String key = (String)keys.next();
            name += key +"_";

        }
        name = name.substring(0,name.length()-1);
        name += "@@";
        Log.e("NAME",  name);
        return name;
    }*/

    private class ProcessJSON extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressdialog = new ProgressDialog(MainActivity.this);
            progressdialog.setMessage("Comprobando");
            progressdialog.show();
        }

        protected String doInBackground(String... strings){

            String stream = null;
            String urlString = strings[0];

            HTTPDataHandler hh = new HTTPDataHandler();
            stream = hh.GetHTTPData(urlString);

            // Return the data from specified url
            return stream;
        }

        protected void onPostExecute(String stream){
            //TextView tv = (TextView) findViewById(R.id.tv);
            codeStatusText.setText(stream);

            /*
                Important in JSON DATA
                -------------------------
                * Square bracket ([) represents a JSON array
                * Curly bracket ({) represents a JSON object
                * JSON object contains key/value pairs
                * Each key is a String and value may be different data types
             */

            //..........Process JSON DATA................
            if(stream !=null){
                try{
                    // Get the full HTTP Data as JSONObject
                    JSONObject reader= new JSONObject(stream);

                    codeStatus = reader.getInt("status");

                    switch (codeStatus){
                        case 0:
                            codeStatusText.setText("Código no válido");
                            break;
                        case 1:
                            codeStatusText.setText("Código válido");
                            break;
                        case 2:
                            codeStatusText.setText("Código ya usado");
                            break;
                        default:
                            codeStatusText.setText("Error");
                            break;
                    }





                    // process other data as this way..............

                }catch(JSONException e){
                    e.printStackTrace();
                }

            } // if statement end

            if (progressdialog != null)
            {
                progressdialog.dismiss();
            }
        } // onPostExecute() end
    } // ProcessJSON class end

}
