package convergencia.vodafone.com.convergencia.config;

/**
 * Contains the common constants used into the app as images uri, bbdd data(tables, version, name)
 * fragments ids and tags, Flurry, New Relic and push notifications config data and the other axa apps Google play´s url 
 * @author Flash2Flash
 *
 */
public class Constants {
	
	public static final String EMPTY_STRING = "";
	public static final int EMPTY_INTEGER = 0;
	
	public static final int SERVICE_STOPPED = 0;
	public static final int SERVICE_RUNNING = 1;
	
	public static final String BROADCAST_RUNNING_ACTIVITY_ACTION = "com.vodafone.xataka.services.local.UploadMediaService.BROADCAST";
	public static final String RUNNING_ACTIVITY_EXTENDED_DATA_STATUS = "com.vodafone.xataka.services.local.UploadMediaService.STATUS";
	
	public static final String IMAGES_DIRECTORY = "Pictures/xataka_photo";

}
