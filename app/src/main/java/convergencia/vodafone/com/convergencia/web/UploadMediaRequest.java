package convergencia.vodafone.com.convergencia.web;

import android.content.Context;

import java.util.HashMap;

/**
 * Executes a request to the service that returns the cache service status
 * @author Flash2Flash
 */
public class UploadMediaRequest extends BaseServiceRequest {
	
	public UploadMediaRequest(Context context,String url,HashMap<String,String> list){
		super(context, url);
		prepareQueryParameters(list);
	}
	
	public UploadMediaRequest(Context context,String url){
		super(context, url);
	}
	
	public HashMap<String,String>getCommonRequestParams(){
		return this.params;
	}
}
