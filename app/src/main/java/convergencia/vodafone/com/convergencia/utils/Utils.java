package convergencia.vodafone.com.convergencia.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import convergencia.vodafone.com.convergencia.config.Config;
import convergencia.vodafone.com.convergencia.config.Constants;

/**
 * Contains common methods used from diferent places into the app
 * @author Juan
 *
 */
public class Utils {
	
	/**
	 * Return md5 from a string given
	 * @param String string given
	 * @return String md5 or null on error
	 */
	public static String md5(String string){
		
		try {
			MessageDigest messageDigest;
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(string.getBytes(Charset.forName("UTF8")));
			final byte[] resultByte = messageDigest.digest();
			
			StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < resultByte.length; ++i){
	        	sb.append(Integer.toHexString((resultByte[i] & 0xFF) | 0x100).substring(1,3));
	        }
	        return sb.toString();
	        
		} catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Generates a new guid
	 * @return String randomUUID 
	 */
	public static String newGuid(){
		return UUID.randomUUID().toString().toUpperCase();
	}
	
	/**
	 * Returns the screen density
	 * @param Context a current activity
	 * @return int density
	 */
	public static int getScreenDensity(Context a){
		DisplayMetrics metrics = new DisplayMetrics();
		((Activity) a).getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.densityDpi;
	}
	
	
	/**
	 * Returns the {@link File} situated into the subfolder inficated and width the name given
	 * @param String subFolder 
	 * @param String name filename
	 * @return {@link File}
	 */
	public static File getOutputMediaFile(String subFolder, String name){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constants.IMAGES_DIRECTORY + File.separator + subFolder);

	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp", "failed to create directory");
	            return null;
	        }
	    }
	    String fileName = new String();
	    if(name.equals(Constants.EMPTY_STRING)){
	    	fileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    }else{
	    	fileName = name;
	    }
	    
	    
	    File mediaFile;
	     
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
        fileName + ".jpg");
	   

	    return mediaFile;
	}
	
	/**
	 * Returns {@link Drawable} from a resource and initialize it if is neccesary
	 * @param context
	 * @param drawable
	 * @param resource
	 * @return {@link Drawable}
	 */
	public static Drawable getDrawableResource(Context context, Drawable drawable, int resource){
		if(drawable == null){
			drawable = context.getResources().getDrawable(resource);
		}
		return drawable;
	}
	
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	/**
	 * Sets a background to a view depending api version
	 * @param view
	 * @param resource
	 */
	public static void setBackgroundToView(View view, Drawable resource){
		
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
        	view.setBackgroundDrawable(resource);
	    }else{
	    	view.setBackground(resource);
	    }
	}
	
	
	/**
	 * Call resizeImg method for common imgs
	 * @param originalPath
	 * @param subFolderDestination
	 * @param imgName
	 */
	public static void resizeAndSaveInImagesFolderImg(String originalPath, String subFolderDestination, String imgName){
		
		resizeImage(originalPath, subFolderDestination, imgName, 800);
	}
	
	
	/**
	 * Resize an image to a width given and save it into its folder
	 * @param originalPath
	 * @param subFolderDestination
	 * @param imgName
	 * @param width
	 */
	private static void resizeImage(String originalPath, String subFolderDestination, String imgName, int width){
		// Get the source image's dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
      
        BitmapFactory.decodeFile(originalPath, options);

        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;
        int desiredWidth = width;
        // Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
        if(desiredWidth > srcWidth)
            desiredWidth = srcWidth;



        // Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
        // from: http://stackoverflow.com/questions/477572/android-strange-out-of-memory-issue/823966#823966
        int inSampleSize = 1;
        while(srcWidth / 2 > desiredWidth){
            srcWidth /= 2;
            srcHeight /= 2;
            inSampleSize *= 2;
        }

        float desiredScale = (float) desiredWidth / srcWidth;

        // Decode with inSampleSize
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(originalPath, options);
        
        ExifInterface exif;
        int orientation = Integer.MIN_VALUE;
		try {
			exif = new ExifInterface(originalPath);
			orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
			sampledSrcBitmap = rotateBitmap(sampledSrcBitmap, orientation);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        
        // Resize
        Matrix matrix = new Matrix();
        matrix.postScale(desiredScale, desiredScale);
        Bitmap scaledBitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
        sampledSrcBitmap = null;

        // Save
        
		try {
			FileOutputStream out = new FileOutputStream(Utils.getOutputMediaFile(subFolderDestination,imgName));
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        scaledBitmap = null;
		} catch (FileNotFoundException e){
			e.printStackTrace();
		}
	}
	
	public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) throws IOException {

	    Matrix matrix = new Matrix();
		switch (orientation) {
		    case ExifInterface.ORIENTATION_NORMAL:
		        return bitmap;
		    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
		        matrix.setScale(-1, 1);
		        break;
		    case ExifInterface.ORIENTATION_ROTATE_180:
		        matrix.setRotate(180);
		        break;
		    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
		        matrix.setRotate(180);
		        matrix.postScale(-1, 1);
		        break;
		    case ExifInterface.ORIENTATION_TRANSPOSE:
		        matrix.setRotate(90);
		        matrix.postScale(-1, 1);
		        break;
		   case ExifInterface.ORIENTATION_ROTATE_90:
		       matrix.setRotate(90);
		       break;
		   case ExifInterface.ORIENTATION_TRANSVERSE:
		       matrix.setRotate(-90);
		       matrix.postScale(-1, 1);
		       break;
		   case ExifInterface.ORIENTATION_ROTATE_270:
		       matrix.setRotate(-90);
		       break;
		   default:
		       return bitmap;
		}
		try {
		    Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		    bitmap.recycle();
		    return bmRotated;
		}
		catch (OutOfMemoryError e) {
		    e.printStackTrace();
		    return null;
		}
	}
	
	/**
	 * Returns a byte array from a {@link File} given
	 * @param file
	 * @return byte[] with the {@link File} information
	 */
	public static byte[] readFile(final File file){
	    if (file.isDirectory())
	    	throw new RuntimeException("Unsupported operation, file "
	    			+ file.getAbsolutePath() + " is a directory");
	    if (file.length() > Integer.MAX_VALUE)
	    	throw new RuntimeException("Unsupported operation, file "
	    			+ file.getAbsolutePath() + " is too big");

	    Throwable pending = null;
	    FileInputStream in = null;
	    final byte buffer[] = new byte[(int) file.length()];
	    try {
	    	in = new FileInputStream(file);
	    	in.read(buffer);
	    } catch (Exception e){
	    	pending = new RuntimeException("Exception occured on reading file "
	    			+ file.getAbsolutePath(), e);
	    } finally {
	    	if (in != null){
	    		try {
	    			in.close();
	    		} catch (Exception e){
	    			if (pending == null){
	    				pending = new RuntimeException(
	    					"Exception occured on closing file" 
	                             + file.getAbsolutePath(), e);
	    			}
	    		}
	    	}
	    	if (pending != null){
	    		throw new RuntimeException(pending);
	    	}
	    }
	    return buffer;
	}
	
	
    /**
     * Gets Application version name from the {@code PackageManager}. It is
     * the visible version showed in G. Play. 
     * 
     * @param appContext Application context got as getApplicationContext()
     * @return Application version name from the {@code PackageManager} or null if 
     * name can not be found.
     */
    public static String getVersionName(Context appContext){
    	try {
            PackageInfo packageInfo = appContext.getPackageManager()
                    .getPackageInfo(appContext.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (NameNotFoundException e){
            // should never happen
//            throw new RuntimeException("Could not get package name: " + e);
        	return null;
        }
    }
    
    /**
     * Gets Application version code from the {@code PackageManager}. It is
     * the incremental integer version, not the versionName shown in G. Play. 
     * 
     * @param appContext Application context got as getApplicationContext()
     * @return Application version code from the {@code PackageManager} or Integer.MIN_VALUE
     * if name could not be found.
     */
    public static int getVersionCode(Context appContext){
    	try {
            PackageInfo packageInfo = appContext.getPackageManager()
                    .getPackageInfo(appContext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e){
            // should never happen
//            throw new RuntimeException("Could not get package name: " + e);
        	return Integer.MIN_VALUE;
        }
    }
    
    /**
     * Deteles recursively a directory given
     * @param dir
     */
    public static void deleteRecursively(File dir){
    	if (dir.isDirectory())
            for (File child : dir.listFiles())
            	deleteRecursively(child);

    	dir.delete();
    }
    
    
    /**
     * Return the non secture url used in services
     * @return {@link String} non secure url base services
     */
    public static String getServicesUrl(){
    	return Config.PROTOCOL + Config.BASE_URL_SERVICES;
    	//return Config.PROTOCOL + Config.BASE_URL_SERVICES;
    }
    
    /**
     * Return the secture url used in services
     * @return {@link String} secure url base services
     */
    public static String getSecureServicesUrl(){
    	return Config.PROTOCOL_SECURE + Config.BASE_SECURE_URL_SERVICES;
    }
}
