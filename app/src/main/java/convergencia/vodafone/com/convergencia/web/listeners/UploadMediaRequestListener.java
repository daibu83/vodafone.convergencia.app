package convergencia.vodafone.com.convergencia.web.listeners;

import android.app.Activity;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Receives Community BCC items or an unique item if an id is passed
 * @author Flash2Flash
 */
public class UploadMediaRequestListener extends BaseRequestListener implements RequestListener<String> {
	
	private Activity currentActivity;
	
	/**
	 * Constructor
	 * @param activity
	 */
	public UploadMediaRequestListener(Activity activity){
		this.currentActivity = activity;
		this.context = activity;
	}
	
	@Override
	public void onRequestFailure(SpiceException spiceException) {
	    Toast.makeText(currentActivity,
				"Error: " + spiceException.getMessage(), Toast.LENGTH_SHORT)
	        .show();
	}
	
	@Override
	public void onRequestSuccess(String serviceResult) {
		currentActivity.setProgressBarIndeterminateVisibility(false);
	}
}
