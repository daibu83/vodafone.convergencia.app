package convergencia.vodafone.com.convergencia.web.listeners;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Base listener class which has common methods used by the other web services listeners 
 * @author Flash2Flash
 */
public class BaseRequestListener {
	Context context;
	
	/**
	 * Check if a response has error
	 * @param jsonObj
	 * @return boolean true if has error or false if not
	 */
	protected boolean serviceHasError(JSONObject jsonObj){
		return jsonObj.opt("error").equals(false) ? false : true;
	}
	
	/**
	 * Returns response result as an object  
	 * @param jsonObj
	 * @return {@link JSONObject} response result
	 */
	protected JSONObject getResult(JSONObject jsonObj){
		try {
			return jsonObj.getJSONObject("result");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns response result as an array
	 * @param jsonObj
	 * @return {@link JSONArray} response result
	 */
	protected JSONArray getArrayResult(JSONObject jsonObj){
		try {
			return jsonObj.getJSONArray("result");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * returns pagination from a list from a {@link JSONObject}
	 * @param jsonObj
	 * @return {@link JSONObject} pagination
	 */
	protected JSONObject getPagination(JSONObject jsonObj){
		try {
			return jsonObj.getJSONObject("pagination");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Prints an error returned from server.
	 * @param tag
	 * @param errorObj
	 * @param ctx
	 */
	protected void printError(String tag, JSONObject errorObj,Context ctx){
		try {
			//error = errorObj.getJSONObject("error");
			String stringError = errorObj.getString("description");
			//If contains | is an alertDialog, else is a toast
			if(stringError.contains("|")){
				String[] arrError = stringError.split("\\|");
				showAlertDialog(arrError[0], arrError[1],ctx);
			}else{
				showToast(errorObj.getString("description"), ctx);
			}
			
			Log.d(tag, errorObj.getString("message"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Shows an error given in a {@link Toast}
	 * @param toast
	 * @param ctx
	 */
	private void showToast(String toast, Context ctx){
        Toast.makeText(ctx, toast, Toast.LENGTH_SHORT).show();
    }
	
	/**
	 * Shows an error given in an {@link AlertDialog}
	 * @param title
	 * @param text
	 * @param context
	 */
	private void showAlertDialog(String title, String text,Context context){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder
			.setTitle(title)
			.setMessage(text)
			.setCancelable(false)
			.setPositiveButton(android.R.string.yes,null);
		AlertDialog alertDialogError = alertDialogBuilder.create();
		alertDialogError.show();
	}
	
	
}
