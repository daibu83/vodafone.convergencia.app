package convergencia.vodafone.com.convergencia.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import convergencia.vodafone.com.convergencia.config.Spf;

/**
 * Class used to sets an app preference
 * @author Juan.sola
 *
 */
public class AppPreferences {
	
	/**
	 * Gets {@link String} preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static String getString(Context context, String preferenceName){
		return (String)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_STRING, Spf.PREFERENCE_TYPE_STRING);
	}
	
	/**
	 * Sets {@link String} preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setString(Context context, String preferenceName, String value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_STRING);
	}
	
	/**
	 * Gets int preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static int getInt(Context context, String preferenceName){
		return (Integer)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_INTEGER, Spf.PREFERENCE_TYPE_INTEGER);
	}
	
	/**
	 * Sets int preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setInt(Context context, String preferenceName, int value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_INTEGER);
	}
	
	/**
	 * Gets long preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static long getLong(Context context, String preferenceName){
		return (Long)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_LONG, Spf.PREFERENCE_TYPE_LONG);
	}
	
	/**
	 * Sets long preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setLong(Context context, String preferenceName, long value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_LONG);
	}
	
	/**
	 * Gets boolean preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static boolean getBoolean(Context context, String preferenceName){
		return (Boolean)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_BOOLEAN, Spf.PREFERENCE_TYPE_BOOLEAN);
	}
	
	/**
	 * Sets boolean preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setBoolean(Context context, String preferenceName, boolean value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_BOOLEAN);
	}
	
	/**
	 * Gets float preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static float getFloat(Context context, String preferenceName){
		return (Float)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_FLOAT, Spf.PREFERENCE_TYPE_FLOAT);
	}
	
	/**
	 * Sets float preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setFloat(Context context, String preferenceName, String value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_FLOAT);
	}
	
	/**
	 * Sets float preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setFloat(Context context, String preferenceName, float value){
		setPreference(context, preferenceName, value, Spf.PREFERENCE_TYPE_FLOAT);
	}
	
	/**
	 * Gets {@link JSONObject} preference
	 * @param context
	 * @param preferenceName
	 * @return preference
	 */
	public static JSONObject getJSONObject(Context context, String preferenceName){
		return (JSONObject)getPreference(context, preferenceName, Spf.PREFERENCE_DEFAULT_JSON_OBJ, Spf.PREFERENCE_TYPE_JSON_OBJ);
	}
	
	/**
	 * Sets {@link JSONObject} preference
	 * @param context
	 * @param preferenceName
	 * @param value
	 */
	public static void setJSONObject(Context context, String preferenceName, JSONObject value){
		setPreference(context, preferenceName, value.toString(), Spf.PREFERENCE_TYPE_JSON_OBJ);
	}
	
	/**
	 * Returns a preference requested from the "getType" method declared before
	 * @param context
	 * @param preferenceName
	 * @param defaultValue
	 * @param preferenceType
	 * @return {@link Object} preference
	 */
	public static Object getPreference(Context context, String preferenceName, Object defaultValue, int preferenceType){
		
		SharedPreferences settings = context.getSharedPreferences(Spf.PREFERENCES_NAME, 0);;
		
		Object obj = new Object();
		switch (preferenceType) {
			case Spf.PREFERENCE_TYPE_STRING:
				obj = settings.getString(preferenceName, (String)defaultValue);
				break;
			case Spf.PREFERENCE_TYPE_INTEGER:
				obj = settings.getInt(preferenceName, (Integer)defaultValue);
				break;
			case Spf.PREFERENCE_TYPE_BOOLEAN:
				obj = settings.getBoolean(preferenceName, (Boolean)defaultValue);
				break;
			case Spf.PREFERENCE_TYPE_FLOAT:
				obj = Float.parseFloat(settings.getString(preferenceName, (String) defaultValue));
				break;
			case Spf.PREFERENCE_TYPE_JSON_OBJ:
				try {
					obj = new JSONObject(settings.getString(preferenceName, (String)defaultValue));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				break;
			case Spf.PREFERENCE_TYPE_LONG:
				obj = settings.getLong(preferenceName, (Long)defaultValue);
				break;
			default:
				obj = null;
				break;
		}
		return obj;
	}
	
	/**
	 * Sets a preference sended by the "setType" method declared before
	 * @param context
	 * @param preferenceName
	 * @param value
	 * @param preferenceType
	 */
	public static void setPreference(Context context, String preferenceName, Object value, int preferenceType){
		
		SharedPreferences settings = context.getSharedPreferences(Spf.PREFERENCES_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		
		switch (preferenceType) {
			case Spf.PREFERENCE_TYPE_STRING:
				editor.putString(preferenceName, (String)value);
				break;
			case Spf.PREFERENCE_TYPE_INTEGER:
				editor.putInt(preferenceName, (Integer)value);
				break;
			case Spf.PREFERENCE_TYPE_BOOLEAN:
				editor.putBoolean(preferenceName, (Boolean)value);
				break;
			case Spf.PREFERENCE_TYPE_FLOAT:
				if(value instanceof Float){
					value = value.toString();
				}
				editor.putString(preferenceName, (String)value);
				break;
			case Spf.PREFERENCE_TYPE_JSON_OBJ:
				editor.putString(preferenceName, (String)value);
				break;
			case Spf.PREFERENCE_TYPE_LONG:
				editor.putLong(preferenceName, (Long)value);
				break;
		}
		
		editor.commit();
	}
}
