package convergencia.vodafone.com.convergencia.web;

import android.content.Context;

import com.octo.android.robospice.request.SpiceRequest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import convergencia.vodafone.com.convergencia.config.Config;
import convergencia.vodafone.com.convergencia.utils.Utils;

/**
 * Contains common methods used in services request children
 * @author Flash2Flash
 */
public class BaseServiceRequest extends SpiceRequest<String> {
	
	protected String url;
	protected Context context;
	protected HashMap<String, String> params;
	

	public BaseServiceRequest(Context context, String url) {
        super(String.class);
        this.context = context;
        this.url = url;
        
        
        params = new HashMap<String, String>();
        prepareCommonQueryParameters();
        
    }

	/**
	 * Load data from network 
	 * @return Json String with the request result
	 */
	@Override
	public String loadDataFromNetwork() throws Exception {
		return HttpClient.SendHttpPost(this.context, this.url, params, false).toString();
	}



	/**
	 * Add to the common parameters list, the extra parameters to execute the request
	 * @param list
	 */
	protected void prepareQueryParameters(HashMap<String, String> list){

		for (Map.Entry<String, String> obj : list.entrySet()) {
			params.put(obj.getKey(), obj.getValue());
		}

	}

	protected void addExtraParams(HashMap<String, String> list){

		for (Map.Entry<String, String> obj : list.entrySet()) {
			if(params.get(obj.getKey()) == null)
				params.put(obj.getKey(), obj.getValue());
		}

	}
	
	/**
	 * Prepare common parameters list to execute the request 
	 */
	protected void prepareCommonQueryParameters(){
		
		String date = String.valueOf(new Date().getTime());
		params.put("md5", Utils.md5(date + Config.MD5_CONCAT));
		params.put("date", date);
		
	}
	
}
