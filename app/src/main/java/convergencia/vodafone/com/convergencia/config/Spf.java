package convergencia.vodafone.com.convergencia.config;

/**
 * Shared preferences fields
 * @author Flash2Flash
 *
 */
public class Spf {

	//AppPreferences config
	public static final String PREFERENCES_NAME = "Axa_Bcc_preferences";
	
	public static final boolean PREFERENCE_DEFAULT_BOOLEAN 		= false;
	public static final String PREFERENCE_DEFAULT_STRING 		= "";
	public static final int PREFERENCE_DEFAULT_INTEGER 			= 0;
	public static final long PREFERENCE_DEFAULT_LONG 			= 0;
	public static final String PREFERENCE_DEFAULT_FLOAT 		= "";
	public static final String PREFERENCE_DEFAULT_JSON_OBJ 		= "[]";
	
	public static final int PREFERENCE_TYPE_BOOLEAN 			= 1;
	public static final int PREFERENCE_TYPE_STRING 				= 2;
	public static final int PREFERENCE_TYPE_INTEGER 			= 3;
	public static final int PREFERENCE_TYPE_FLOAT 				= 4;
	public static final int PREFERENCE_TYPE_JSON_OBJ 			= 5;
	public static final int PREFERENCE_TYPE_LONG 				= 6;
	
	
	//Tokens fields
	public static final String PREFERENCE_TOKEN 				= "access_token";
	public static final String PREFERENCE_GPLUS_TOKEN 			= "google_plus_access_token";
	
	//User fields
	public static final String USER_ID 							= "axa_bcc_user_id";
	public static final String USER_FIRST_NAME 					= "axa_bcc_user_first_name";
	public static final String USER_LAST_NAME 					= "axa_bcc_user_laset_name";
	public static final String USER_EMAIL 						= "axa_bcc_user_email";
	public static final String USER_PROVINCE_ID 				= "axa_bcc_user_province_id";
	public static final String USER_PROVINCE 					= "axa_bcc_user_province";
	
	public static final String USER_GENDER 						= "axa_bcc_user_gender";
	
	public static final String USER_LEVEL 						= "axa_bcc_user_level";
	public static final String USER_REGISTRATION_ORIGIN 		= "axa_bcc_user_reg_origin";
	
	public static final String USER_LAT 					= "axa_bcc_user_lat";
	public static final String USER_LNG 					= "axa_bcc_user_lng";
	
	
}
