package convergencia.vodafone.com.convergencia.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import convergencia.vodafone.com.convergencia.config.Config;

/**
 * Class used to gets images from network, first search it into the local system and 
 * if it does not found it, tries to download
 * @author Juan.sola
 *
 */
public class DownloadAndReadImage 
{
	private static final String TAG = "DownloadAndReadImage";
	
    String strURL;
    String name;
    String imgSizeFolder;
    int routeId;
    String finalPath;
    Bitmap bitmap=null;
    
    // pass image url and Pos for example i:
    public DownloadAndReadImage(String url, String imgSizeFolder, int routeId, String name, String sectionDir)
    {
    	this.strURL=url;
        this.name=name;
        this.imgSizeFolder = imgSizeFolder;
        this.routeId = routeId;
        this.finalPath = sectionDir + File.separator + routeId + File.separator + imgSizeFolder;
    }
    
    // pass image url and Pos for example i:
    public DownloadAndReadImage(String url, String name, String sectionDir)
    {
        this.strURL=url;
        this.name=name;
        this.finalPath = sectionDir;
    }
    
    /**
     * Downloads and read an image when it has been downloaded
     * @return {@link Bitmap} image
     */
    public Bitmap getBitmapImage()
    {
    	downloadBitmapImage();
    	return readBitmapImage();
    }
    
    /**
     * Downloads an image from network
     */
    void downloadBitmapImage()
    {
        InputStream input;
        long t = System.currentTimeMillis();
        Log.d("DownloadAndReadImage", "New Request GET. URL:" + strURL);
        try {

            URL url = new URL(strURL);
            URLConnection urlConn = url.openConnection();
            urlConn.setConnectTimeout(Config.HTTP_CONNECTION_TIMEOUT_IMGS);
            urlConn.setReadTimeout(Config.HTTP_SOCKET_TIMEOUT_IMGS);
            input = urlConn.getInputStream();
            byte[] buffer = new byte[1500];
            
            
            File directory = new File(finalPath);
            
            if(!directory.exists())
            {
            	directory.mkdirs();
            } 
            
			try {
				File nomedia = new File(finalPath, ".nomedia");
				if (!nomedia.exists()) {
					nomedia.createNewFile();
				}
			} catch (IOException e) {
			}
            
            OutputStream output = new FileOutputStream( finalPath + File.separator + name);
            try 
            {     
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) 
                {
                    output.write(buffer, 0, bytesRead);
                }
            } 
            finally 
            {
                output.close();
                buffer=null;
            }
        } catch (ConnectTimeoutException e) {
			Log.d(TAG, "ConnectTimeoutException - HTTPResponse received in [" + (System.currentTimeMillis() - t) + "ms]. URL:" + strURL);
		} catch (final SocketTimeoutException e) {
			Log.d(TAG, "SocketTimeoutException - HTTPResponse received in [" + (System.currentTimeMillis() - t) + "ms]. URL:" + strURL);
		} catch(Exception e)
        {
			Log.d(TAG, "Exception - [" + (System.currentTimeMillis() - t) + "ms]. Service:" + strURL);
        	e.printStackTrace();
            //Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_LONG).show();
        }
    } 
    
    /**
     * Reads an image from local system
     * @return {@link Bitmap} image
     */
    public Bitmap readBitmapImage()
    {
    	String pathInSD = finalPath + File.separator + name;
        BitmapFactory.Options bOptions = new BitmapFactory.Options();
        bOptions.inTempStorage = new byte[16*1024];
                
            bitmap = BitmapFactory.decodeFile(pathInSD, bOptions);
            
        return bitmap;
    }
    
    /**
     * Check if is needed download an image an does it
     */
    public void downloadIfIsNeeded(){
    	File file = new File(finalPath + File.separator + name);
    	if(!file.isFile()){
    		downloadBitmapImage();
    	}
    }    
}