package convergencia.vodafone.com.convergencia.config;


/**
 * Contains the common parameters used into the app as url services connection, facebook app id or oauth data
 * @author Flash2Flash
 *
 */
public class Config {
	
	//BETA
	public static final String PROTOCOL = "http://";
	public static final String PROTOCOL_SECURE = "http://";

	//public static final String BASE_SECURE_URL_SERVICES = "convergencia.f2fbeta.com/";
	//public static final String BASE_URL_SERVICES = "convergencia.f2fbeta.com/";
	public static final String BASE_SECURE_URL_SERVICES = "cocinandojuntosmadrid.com/";
	public static final String BASE_URL_SERVICES = "cocinandojuntosmadrid.com/";

	public static final int HTTP_CONNECTION_TIMEOUT = 20000;
	public static final int HTTP_SOCKET_TIMEOUT = 20000;
	public static final int HTTP_CONNECTION_TIMEOUT_IMGS = 6000;
	public static final int HTTP_SOCKET_TIMEOUT_IMGS = 6000;
	
	public static final String MD5_CONCAT = "Vodafone_2014";
}
